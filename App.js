import React, { useState, useEffect } from 'react';
import { TouchableHighlight, Button, Text, View, FlatList, StyleSheet, TouchableOpacity, TextInput, ActivityIndicator, Image, Vibration } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

function HomeScreen({ navigation }) {
  const [searchInput, setSearchInput] = useState('');
  const [feed, setFeed] = useState([]);
  const ONE_SECOND_IN_MS = 15;
  useEffect(() => {
    fetch('https://aurora-django-app.herokuapp.com/feed?feed_count=0')
      .then((re) => re.json())
      .then((re) => {
        setFeed(re.response);
        console.log(re.response);
      })
  }, []);
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <TouchableOpacity style={styles.arrow} onPress={() => navigation.navigate('Details')}>
        <Icon name="caretright"></Icon>
      </TouchableOpacity>
      <View style={styles.mainPostView}>
        {feed.length < 1 ?
          <ActivityIndicator size={"large"} color={"#2FBBD0"} />
          :
          <FlatList
            data={feed}
            keyExtractor={(item, index) => { return item.post_id.toFixed() }}
            renderItem={({ item, index }) => (
              <View style={styles.postView}>

                <View style={styles.postTitle}>
                  <View style={styles.imageView}>
                    <TouchableOpacity onPress={() => Vibration.vibrate()}>
                      <Image style={styles.artistPhoto} source={{ uri: item.artist_photo }} />
                    </TouchableOpacity>
                    <View style={styles.titleView}>
                      <Text style={styles.artist_name}>{item.post_artist}</Text>
                      <Text style={styles.post_title}>{item.post_title}</Text>
                    </View>
                  </View>
                </View>
                <View>
                </View>
                <View>
                    <Image style={styles.coverPhoto} source={{ uri: item.cover_poto }} />
                </View>
              </View>
            )}
          />
        }
      </View>

    </View>
  );
}

function DetailsScreen({navigation}) { 
  const [feed, setFeed] = useState([]);
  const ONE_SECOND_IN_MS = 15;
  useEffect(() => {
    fetch('https://aurora-django-app.herokuapp.com/feed?feed_count=3')
      .then((re) => re.json())
      .then((re) => {
        setFeed(re.response);
        console.log(re.response);
      })
  }, []);
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <TouchableOpacity style={styles.arrow} onPress={() => navigation.navigate('Page 1')}>
        <Icon name="caretleft"></Icon>
      </TouchableOpacity>
      <View style={styles.mainPostView}>
        {feed.length < 1 ?
          <ActivityIndicator size={"large"} color={"#2FBBD0"} />
          :
          <FlatList
            data={feed}
            keyExtractor={(item, index) => { return item.post_id.toFixed(1) }}
            renderItem={({ item, index }) => (
              <View style={styles.postView}>
                <View style={styles.postTitle}>
                  <View style={styles.imageView}>
                    <Image style={styles.artistPhoto} source={{ uri: item.artist_photo }} />
                    <View style={styles.titleView}>
                      <Text style={styles.artist_name}>{item.post_artist}</Text>
                      <Text style={styles.post_title}>{item.post_title}</Text>
                    </View>
                  </View>
                </View>
                <View>
                </View>
                <View>
                  <Image style={styles.coverPhoto} source={{ uri: item.cover_poto }} />
                </View>
              </View>
            )}
          />
        }
      </View>
    </View>
  );
}
const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <View style={styles.Header}>
      <View style={styles.arrow}>

      </View>

      <NavigationContainer>
        <Stack.Navigator initialRouteName="Home">
          <Stack.Screen name="Page 1" component={HomeScreen} />
          <Stack.Screen name="Details" component={DetailsScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </View>
  );
}

export default App
const styles = StyleSheet.create({
  arrow: {
    marginLeft: '80%',
    marginTop: 30,
    width: '100%'
  },
  Header: {
    flex: 1,
    height: '100%',
    width: '100%'
  },
  iconView: {
    width: 50,
    flex: 1,
  },
  post_title: {
    fontSize: 11,
    color: '#989898',
  },
  artist_name: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  mainView: {
    flex: 1,
  },
  titleView: {
    marginLeft: 15,

  },
  Heading: {
    fontSize: 32,
    marginTop: 30,
    marginLeft: 15,
    fontWeight: 'bold',
  },
  TextInputView: {
    display: 'flex',
    alignItems: 'center',
  },
  mainPostView: {
    width: '100%',
  },
  postTitle: {
    width: '90%',
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  postView: {
    width: '100%',
    alignItems: 'center',
    marginTop: 20,
  },
  artistPhoto: {
    backgroundColor: 'rgba(0,0,0,0.06)',
    width: 50,
    height: 50,
    borderRadius: 50,
  },
  imageView: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  coverPhoto: {
    width: 370,
    height: 180,
    backgroundColor: 'rgba(0,0,0,0.06)',
    marginTop: 20,
    borderRadius: 10,
  }
})